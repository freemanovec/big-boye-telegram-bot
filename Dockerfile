FROM docker.io/python:3.9-buster

WORKDIR /app
ADD . /app
RUN python3 -m pip install --user --upgrade pip && python3 -m pip install --user --upgrade -r ./requirements.txt
ENTRYPOINT [ "python3", "-u", "/app/main.py" ]
